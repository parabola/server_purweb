# Where are we?
topdir := $(dir $(lastword $(MAKEFILE_LIST)))
web-cache = $(topdir)/web-cache
www = $(web-cache)/www
artwork = $(web-cache)/artwork
branding = $(artwork)/official-2013/optimized-text

targets = web/html/images/feed-icon-14x14.png

all: $(targets)
clean:
	rm -f -- $(targets)
.PHONY: all clean

$(www)/http/%:
	mkdir -p '$(@D)'
	wget 'http://$*' -O '$@'
	touch '$@'

$(www)/https/%:
	mkdir -p '$(@D)'
	wget 'https://$*' -O '$@'
	touch '$@'

web/html/images/feed-icon-14x14.png: $(www)/http/www.feedicons.com/images/feed-icon-14x14.png
	ln -srf $< $@

.DELETE_ON_ERROR:
.SECONDARY:
